using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
public class NetworkPlayer : NetworkBehaviour
{

    public Transform Root;
    public Transform Head;
    public Transform LHand;
    public Transform RHand;

    public Renderer[] meshToDisable;



    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        
        foreach (Renderer r in meshToDisable)
        {
            if (IsOwner)
            {
                r.enabled = false;
            }
           
        }
    }


    void Update()
    {
        if (IsOwner)
        {
            Root.position = VRRigReferences.Singelton.Root.position;
            Root.rotation = VRRigReferences.Singelton.Root.rotation;


            Head.position = VRRigReferences.Singelton.Head.position;
            Head.rotation = VRRigReferences.Singelton.Head.rotation;


            LHand.position = VRRigReferences.Singelton.LHand.position;
            LHand.rotation = VRRigReferences.Singelton.LHand.rotation;


            RHand.position = VRRigReferences.Singelton.RHand.position;
            RHand.rotation = VRRigReferences.Singelton.RHand.rotation;
        }
        
    }
}
