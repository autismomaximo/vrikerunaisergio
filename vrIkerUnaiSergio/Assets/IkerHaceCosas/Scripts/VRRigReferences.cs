using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRRigReferences : MonoBehaviour
{

    public static VRRigReferences Singelton;

    public Transform Root;
    public Transform Head;
    public Transform LHand;
    public Transform RHand;


    private void Awake()
    {
        Singelton = this; 
    }

}
