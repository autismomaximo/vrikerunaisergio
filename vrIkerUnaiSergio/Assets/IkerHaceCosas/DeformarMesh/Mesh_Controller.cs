using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class Mesh_Controller : MonoBehaviour
{
    [Range(1.5f, 5f)]
    public float radius = 2f;
    [Range(1.5f, 5f)]
    public float deformationStrenght = 2f;

    private Mesh mesh;
    private Vector3[] vertices;
    private Vector3[] verticesModificados;
    private bool flag;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        verticesModificados = mesh.vertices;

        flag = false;
        StartCoroutine("Intangible");

    }

    public void recalcularMesh()
    {
        print("Recalculating normals");
        mesh.vertices = verticesModificados;
        GetComponent<MeshCollider>().sharedMesh = mesh;
        mesh.RecalculateNormals();
    }

    private void OnCollisionEnter(Collision collision)
    {

        //print("A");

        if (flag)
        {
            flag = false;

            if (collision.transform.tag.Equals("Blade"))
            {
                for (int v = 0; v < verticesModificados.Length; v++)
                {
                    Vector3 distancia = verticesModificados[v] - collision.GetContact(0).point;

                    float smoothinFactor = 2f;
                    float force = deformationStrenght / (1f + collision.GetContact(0).point.sqrMagnitude);

                    if (distancia.sqrMagnitude < radius)
                    {
                        verticesModificados[v] = verticesModificados[v] + (collision.relativeVelocity.normalized * force) / smoothinFactor;
                    }


                }
            }

            recalcularMesh();
            StartCoroutine("Intangible");
        }
        

    }

    IEnumerator Intangible()
    {
        yield return new WaitForSeconds(0.1f);
        flag = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
