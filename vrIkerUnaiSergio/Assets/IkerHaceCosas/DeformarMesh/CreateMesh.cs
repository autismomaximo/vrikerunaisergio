using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Unity.AI.Navigation;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.Interaction.Toolkit.AffordanceSystem.State;
using static UnityEditor.Progress;
using static UnityEditor.Searcher.SearcherWindow.Alignment;

public class CreateMesh : MonoBehaviour
{

    [Range(1f, 100f)]
    public int AfectedNeighbors = 1;
    [Range(0f, 5f)]
    public float deformationStrenght = 2f;
    
    public Vector2 meshSize;
    public int meshResolution;

    protected Mesh mesh;
    protected MeshFilter meshFilter;
 
    private Vector3[] verticesModificados;
    public bool flag;

    public GameObject mPoint;
    public float floorLimit;


    void Start()
    {
        mesh = new Mesh();
        mesh.name = "NEWMesh";

        meshFilter = gameObject.AddComponent<MeshFilter>();

        meshFilter.sharedMesh = mesh;

        crearMesh(meshSize, meshResolution);
        gameObject.AddComponent<MeshCollider>();


        verticesModificados = mesh.vertices;

        flag = true;
        StartCoroutine("Intangible");
        this.GetComponentInChildren<NavMeshSurface>().BuildNavMesh();
    }


    public void crearMesh(Vector2 size, int resolution)
    {
        float xPerStep = size.x / resolution;
        float yPerStep = size.y / resolution;

        int point = 0;
        Vector3[] vertices = new Vector3[(resolution + 1) * (resolution + 1)];
        for (int x = 0; x < (resolution + 1); x++)
        {
            for (int z = 0; z < (resolution + 1); z++)
            {
                point++;
                vertices[x * (resolution + 1) + z] = new Vector3(x*xPerStep, 0, z*yPerStep);
            }               
        }

        mesh.vertices = vertices;


        int[] triangles = new int[resolution * resolution * 2 * 3];
        for (int x = 0; x < resolution; x++)
            for (int z = 0; z < resolution; z++)
            {
                triangles[x * (resolution * 6) + z * 6] = x * (resolution + 1) + z;
                triangles[x * (resolution * 6) + z * 6 + 1] = (x + 1) * (resolution + 1) + z + 1;
                triangles[x * (resolution * 6) + z * 6 + 2] = (x + 1) * (resolution + 1) + z;

                triangles[x * resolution * 6 + z * 6 + 3] = x * (resolution + 1) + z;
                triangles[x * resolution * 6 + z * 6 + 4] = x * (resolution + 1) + z + 1;
                triangles[x * resolution * 6 + z * 6 + 5] = (x + 1) * (resolution + 1) + z + 1;
            }
        mesh.triangles = triangles;


        mesh.RecalculateNormals();
        mesh.RecalculateBounds();


        Vector2[] uvs = new Vector2[vertices.Length];

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }
        mesh.uv = uvs;



        //remove one square idk...

    }

    public void recalcularMesh()
    {
        mesh.vertices = verticesModificados;
        GetComponent<MeshCollider>().sharedMesh = mesh;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        this.GetComponentInChildren<NavMeshSurface>().BuildNavMesh();
    }


    public Vector3Int IndexToCoordinates(int index)
    {
        int x = (int)(index / meshSize.x);
        int y = (int)(index % meshSize.y);

        return new Vector3Int(x, y, 0);
    }

    public int CoorToIndex(Vector3Int coord)
    {
        float res = coord.x * meshSize.x + coord.y;

        return (int)res;
    }

    private void OnCollisionEnter(Collision collision)
    {

        print("TOC");

        if (flag)
        {
            flag = false;

            print("FLAG");

            if (collision.gameObject.tag.Equals("Blade"))
            {

                RaycastHit hit;

                float force = deformationStrenght;

                print("BEAM");

                if (Physics.Raycast(collision.gameObject.transform.position, collision.relativeVelocity.normalized, out hit, 100))
                {

                    //print("BONK");
                    //print("Indice: " + hit.triangleIndex * 3);
                    //print("Lenght: " + mesh.triangles.Length);

                    if (hit.triangleIndex * 3 < mesh.triangles.Length && hit.triangleIndex * 3 > 0)
                    {
                        int VertIndex = mesh.triangles[hit.triangleIndex * 3] + meshResolution + 1;

                        if (VertIndex < verticesModificados.Length && VertIndex > 0)
                        {

                            try
                            {
                                if(verticesModificados[VertIndex].y > floorLimit)
                                    verticesModificados[VertIndex] = verticesModificados[VertIndex] + (collision.relativeVelocity.normalized * force);

                                int h = 0;
                                for (int i = 1; i <= AfectedNeighbors; i++)
                                {
                                    h = 0;

                                    float lesserForce = math.max((force - (force * (i * 100 / AfectedNeighbors))/100), 0);

                                    if (VertIndex + i < verticesModificados.Length && verticesModificados[VertIndex + i].y > floorLimit)
                                        verticesModificados[VertIndex + i] += collision.relativeVelocity.normalized * lesserForce;
                                    if (VertIndex - i < verticesModificados.Length && verticesModificados[VertIndex - i].y > floorLimit)
                                        verticesModificados[VertIndex - i] += collision.relativeVelocity.normalized * lesserForce;
                                    if (VertIndex - i - meshResolution * i < verticesModificados.Length && verticesModificados[VertIndex - i - meshResolution * i].y > floorLimit)
                                        verticesModificados[VertIndex - i - meshResolution * i] += collision.relativeVelocity.normalized * lesserForce;
                                    if (VertIndex + i + meshResolution * i < verticesModificados.Length && verticesModificados[VertIndex + i + meshResolution * i].y > floorLimit)
                                        verticesModificados[VertIndex + i + meshResolution * i] += collision.relativeVelocity.normalized * lesserForce;


                                    for (int j = i; j <= AfectedNeighbors; j++)
                                    {

                                        h++;

                                        float lessererForce = math.max((force - (force * ((i+h) * 100 / AfectedNeighbors))/100), 0);

                                        if ((VertIndex - i - meshResolution * i) + h < verticesModificados.Length && verticesModificados[(VertIndex - i - meshResolution * i) + h].y > floorLimit)
                                            verticesModificados[(VertIndex - i - meshResolution * i) + h] += collision.relativeVelocity.normalized * lessererForce;
                                        if ((VertIndex - i - meshResolution * i) - h < verticesModificados.Length && verticesModificados[(VertIndex - i - meshResolution * i) - h].y > floorLimit)
                                            verticesModificados[(VertIndex - i - meshResolution * i) - h] += collision.relativeVelocity.normalized * lessererForce;
                                        if ((VertIndex + i + meshResolution * i) + h < verticesModificados.Length && verticesModificados[(VertIndex + i + meshResolution * i) + h].y > floorLimit)
                                            verticesModificados[(VertIndex + i + meshResolution * i) + h] += collision.relativeVelocity.normalized * lessererForce;
                                        if ((VertIndex + i + meshResolution * i) - h < verticesModificados.Length && verticesModificados[(VertIndex + i + meshResolution * i) - h].y > floorLimit)
                                            verticesModificados[(VertIndex + i + meshResolution * i) - h] += collision.relativeVelocity.normalized * lessererForce;

                                    }


                                }

                                recalcularMesh();


                            }
                            catch (Exception e)
                            {

                            }
                            
                        }

                    }
                }
            }

            StartCoroutine("Intangible");

        }


    }

    IEnumerator Intangible()
    {
        yield return new WaitForSeconds(0.1f);
        flag = true;
    }

    void Update()
    {

    }


}
