using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class mVoxel : MonoBehaviour
{

    public Vector3 terrainSize;
    public float voxelSize;
    public GameObject voxel;

    private mVoxelStruct[,,] voxels;

    // Start is called before the first frame update
    void Start()
    {

        voxels = new mVoxelStruct[(int)terrainSize.x, (int)terrainSize.y, (int)terrainSize.z];
        InitializeVoxelArr();
    }

    void InitializeVoxelArr()
    {

        for (int i = 0; i < (int)terrainSize.x; i++)
        {
            for(int j = 0; j < (int)terrainSize.y; j++)
            {
                for(int k = 0; k < (int)terrainSize.z; k++)
                {

                    Vector3 position = new Vector3((this.transform.position.x + i * voxelSize), (this.transform.position.y + j * voxelSize), (this.transform.position.z + k * voxelSize));

                    voxels[i, j, k] = new mVoxelStruct(position, true);

                }
            }
        }

        createExterior();

    }

    void createExterior()
    {

        for (int z = 0; z < terrainSize.z; z++)
        {
            for (int x = 0; x < terrainSize.x; x++)
            {
                createPhysicalVoxels(x, (int)terrainSize.y - 1, z);
            }
        }

        /*for (int x = 0; x < terrainSize.x; x++)
        {
            for (int y = 0; y < terrainSize.y; y++)
            {
                //createPhysicalVoxels(x, y, 0);
                createPhysicalVoxels(x, y, (int)terrainSize.z - 1);
            }
        }

        /*for (int y = 0; y < terrainSize.y; y++)
        {
            for (int z = 1; z < terrainSize.z-1; z++)
            {
                createPhysicalVoxels(0, y, z);
                createPhysicalVoxels((int)terrainSize.x - 1, y, z);
            }
        }

        for (int z = 1; z < terrainSize.z; z++)
        {
            for (int x = 1; x < terrainSize.x-1; x++)
            {
                createPhysicalVoxels(x, 0, z);
                createPhysicalVoxels(x, (int)terrainSize.y - 1, z);
            }
        }*/
    }

    void createPhysicalVoxels(int x, int y, int z)
    {

        mVoxelStruct data = voxels[x, y, z];

        if (data.isActive)
        {
            GameObject newVoxel = Instantiate(voxel);
            newVoxel.transform.localScale *= voxelSize;
            newVoxel.transform.position = data.position;
        }

    }




    // Update is called once per frame
    void Update()
    {
        
    }
}
