using UnityEngine;

// Define a simple Voxel struct
public struct mVoxelStruct
{
    public Vector3 position;
    public bool isActive;

    public mVoxelStruct(Vector3 position, bool isActive = true)
    {
        this.position = position;
        this.isActive = isActive;
    }
}