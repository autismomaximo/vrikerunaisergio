using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretRadius : MonoBehaviour
{

    public Turret myTurret;
    // Start is called before the first frame update
    void Start()
    {
        myTurret = this.transform.parent.GetComponent<Turret>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.tag == "enemy")
        //{
        //    myTurret.targetEnemy = other.gameObject.transform;
        //    myTurret.cambiarEstado(Turret.turretStates.shooting);
        //}

        if (other.gameObject.GetComponent<combatCharacter>() != null && other.gameObject.GetComponent<combatCharacter>().myTeam == combatCharacter.team.enemies)
        {
            myTurret.targetEnemy = other.gameObject.transform;
            myTurret.cambiarEstado(Turret.turretStates.shooting);
        }
    }

}
