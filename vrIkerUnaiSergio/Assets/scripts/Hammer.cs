using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (this.GetComponent<ParticleSystem>() != null)
        {
            this.GetComponent<ParticleSystem>().Play();
        }
        Debug.Log("coll "+this.gameObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.GetComponent<ParticleSystem>() != null)
        {
            this.GetComponent<ParticleSystem>().Play();
        }
        Debug.Log("trigg " + this.gameObject.name);
    }
}
