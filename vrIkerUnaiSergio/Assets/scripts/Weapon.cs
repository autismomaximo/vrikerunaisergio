using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Weapon : MonoBehaviour
{
    public combatCharacter ownerCharacter;
    //cosas disparo
    //public float tiempoCongelacion = 3;
    public enum state { idle, disparando, recargando };
    public state estado;
    public float cadenciaTime;
    public float ReloadTime;
    public float rango;
    public float balasActuales;
    public float balasMaximas;
    public float dispersion;
    public float numBalasDisparo;
    public float damage;

    //cosas extra
    public LayerMask mask;
    public LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        /*
        //pongo el mesh
        this.miMesh = this.GetComponent<Mesh>();
        this.miMesh = statsArma.mesh;
        */

        //pongo en mis stats los de mi scriptableObject
        //this.cadenciaTime = statsArma.cadenciaTime;
        //this.ReloadTime = statsArma.ReloadTime;
        //this.rango = statsArma.rango;
        //this.balasMaximas = statsArma.balasMaximas;
        //this.dispersion = statsArma.dispersion;
        //this.numBalasDisparo = statsArma.numBalasDisparo;
        //this.damage = statsArma.damage;

        //inicializo el resto de cosas
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        this.balasActuales = this.balasMaximas;
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetMouseButton(0))
        //{
        //    if (this.balasActuales > 0 && estado == state.idle)
        //    {
        //        shoot();
        //    }
        //    else if (estado == state.idle)
        //    {
        //        StartCoroutine(reload());
        //    }
        //}

        //if (Input.GetKeyDown(KeyCode.R) && estado == state.idle)
        //{
        //    StartCoroutine(reload());
        //}
        

    }

    public void assignOwner(GameObject owner)
    {
        if(owner != null && owner.GetComponent<combatCharacter>() != null)
        {
            this.ownerCharacter = owner.GetComponent<combatCharacter>();
        }
        else
        {
            Debug.Log("jijijija");
            ////si entro aqui es porque lo ha grabbeado player
            //if (this.gameObject.transform.parent.transform.parent != null && this.gameObject.transform.parent.transform.parent.GetComponent<combatCharacter>() != null) 
            //{
            //    this.ownerCharacter = this.gameObject.transform.parent.transform.parent.GetComponent<combatCharacter>();
            //}
        }
        
    }

    public void unassignOwner()
    {
        this.ownerCharacter.unassignWeapon();
        this.ownerCharacter = null;
    }

    public void shoot()
    {
        if (this.balasActuales > 0 && estado == state.idle)
        {
            shootForReal();
        }
        else if (estado == state.idle)
        {
            StartCoroutine(reload());
        }
    }

    private void shootForReal()
    {
        if (estado == state.idle)
        {
            //print("---BADGUYPEW---");

            //activa el linerenderer
            lineRenderer.enabled = true;

            for (int i = 0; i < numBalasDisparo; i++)
            {
                //guarda la informaci sobre la collisio amb la que ha xocat el raycast
                RaycastHit hit;

                float dispersionX = Random.Range(-dispersion, dispersion);
                float dispersionY = Random.Range(-dispersion, dispersion);
                float dispersionZ = Random.Range(-dispersion, dispersion);


                Vector3 finalLinea = new Vector3(this.transform.position.x + (this.transform.forward.x + dispersionX) * rango, this.transform.position.y + (this.transform.forward.y + dispersionY) * rango, this.transform.position.z + (this.transform.forward.z + dispersionZ) * rango);

                //Vector3 pruebaParticula = new Vector3(this.transform.forward.x + dispersionX, this.transform.forward.y + dispersionY, this.transform.forward.z + dispersionZ) * 100;

                //Physics.RaycastAll
                Debug.DrawRay(this.transform.position, new Vector3(this.transform.forward.x + dispersionX, this.transform.forward.y + dispersionY, this.transform.forward.z + dispersionZ) * rango, Color.blue, 2f);
                //Debug.DrawRay(miPadreEnemigo.transform.position, new Vector3(miPadreEnemigo.transform.forward.x + dispersionX, miPadreEnemigo.transform.forward.y + dispersionY, miPadreEnemigo.transform.forward.z + dispersionZ) * rango, Color.red, 2f);

                //dibuja particula

                //GameObject newVEGO = ParticulaPool.SharedInstance.GetPooledObject();

                //if (newVEGO != null)
                //{
                //    newVEGO.SetActive(true);
                //    //GameObject newVEGO = Instantiate(miVE);

                //    newVEGO.transform.position = this.transform.position;

                //    newVEGO.transform.rotation = this.transform.rotation;

                //    VisualEffect newVE = newVEGO.GetComponent<VisualEffect>();

                //    newVE.SetVector3("direccion", pruebaParticula);

                //    newVE.Play();

                //    StartCoroutine(desactivarParticula(newVEGO));
                //}
                //else
                //{
                //    Debug.Log("no hay suficientes visual effects en la pool");
                //}



                //esta funcion se llama si el raycast choca con algo
                if (Physics.Raycast(this.transform.position, new Vector3(this.transform.forward.x + dispersionX, this.transform.forward.y + dispersionY, this.transform.forward.z + dispersionZ) * rango, out hit, rango, mask))
                {
                    GameObject GOHit = hit.transform.gameObject;

                    finalLinea = hit.point;

                    if (GOHit.GetComponent<combatCharacter>() != null)
                    {
                        GOHit.GetComponent<combatCharacter>().getHurt(this.ownerCharacter);
                    }
                }

                Vector3[] positions = new Vector3[]
                {
            this.transform.position,
            finalLinea
                };

                lineRenderer.SetPositions(positions);
            }

            if (this.balasActuales - numBalasDisparo < 0)
            {
                this.balasActuales = 0;
            }
            else
            {
                this.balasActuales = this.balasActuales - numBalasDisparo;
            }

            StartCoroutine(cooldown(cadenciaTime));
            StartCoroutine(borraLinea());
        }
    }


    IEnumerator desactivarParticula(GameObject newVEGO)
    {
        yield return new WaitForSeconds(5);

        newVEGO.SetActive(false);

    }

    IEnumerator borraLinea()
    {
        yield return new WaitForSeconds(0.1f);
        lineRenderer.enabled = false;
    }


    IEnumerator reload()
    {
        Debug.Log("recargando");
        this.estado = state.recargando;
        yield return new WaitForSeconds(ReloadTime);
        this.balasActuales = balasMaximas;
        this.estado = state.idle;
    }

    IEnumerator cooldown(float tiempoAEsperar)
    {
        this.estado = state.disparando;
        yield return new WaitForSeconds(tiempoAEsperar);
        this.estado = state.idle;
    }

}
