using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAttack : MonoBehaviour
{
    public combatCharacter myEnemy;

    // Start is called before the first frame update
    void Start()
    {
        myEnemy = this.gameObject.GetComponentInParent<combatCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<combatCharacter>() != null)
        {
            Debug.Log(other.gameObject);
            other.gameObject.GetComponent<combatCharacter>().getHurt(myEnemy);
        }
    }

}
