using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float spawnTimer = 1;
    public float timeToStop = 10;
    public bool stoppableSpawner;
    private Coroutine spawnCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        spawnCoroutine = StartCoroutine(Spawn());
        if (stoppableSpawner)
        {
            StartCoroutine(itsTimeToStop());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawn()
    {
        while(true) 
        {
            Instantiate(enemyPrefab, this.transform.position, this.transform.rotation);
            yield return new WaitForSeconds(spawnTimer);
        }
    }

    IEnumerator itsTimeToStop()
    {
        yield return new WaitForSeconds(timeToStop);
        stopSpawning();
    }

    public void stopSpawning()
    {
        StopCoroutine(spawnCoroutine);
    }

}
