using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class combatCharacter : MonoBehaviour
{

    public float Hp;
    public float maxHp;
    public float shield;
    public float maxShield;
    public float atk;
    public float def;
    public team myTeam;
    public Weapon myWeapon;

    public enum team
    {
        players, enemies
    }

    public states myState = states.idle;
    public enum states
    {
        idle, dead
    }

    private float timeToStartRegen = 4000;
    private float timePassedSinceRecievedDamage = 0;
    private float secondsBetweenRegens = 1;
    private float shieldToRegeneratePerLoop = 1;
    private Coroutine regenCoroutine;

    public GameEvent playerDeathEvent;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        changeShield(maxShield);
        changeHp(maxHp);
    }

    // Update is called once per frame
    void Update()
    {
        //regeneracion de escudo si no recibes da�o en x segundos
        if (myTeam == team.players && myState != states.dead && shield < maxShield)
        {
            if (timePassedSinceRecievedDamage >= timeToStartRegen)
            {
                startRegen();
            }
            else
            {
                timePassedSinceRecievedDamage += 1;
            }
        }
    }

    public virtual void getHurt(combatCharacter chara)
    {
        if (this.myTeam == team.enemies && chara.myTeam == team.enemies)
        {
            //no hace da�o
        }
        else
        {
            Debug.Log(this.gameObject.name + " took damage");

            stopRegen();
            float atkDefDmg = (chara.atk - this.def);
            if (atkDefDmg <= 0)
                atkDefDmg = 1;

            if (myTeam == team.players && shield > 0)
            {
                float finalShield = this.shield - atkDefDmg;

                changeShield(finalShield);
            }
            else
            {
                float finalHp = this.Hp - atkDefDmg;

                changeHp(finalHp);
            }
        }
    }

    public void die()
    {
        Debug.Log(this.gameObject.name+" has died");
        myState = states.dead;
        if (this.myTeam == team.enemies)
        {
            Destroy(this.gameObject);
            //temporal, se debe cambiar por los puntos al minar
            gameManager.addPoints(100);
        }
        else
        {
            this.gameObject.GetComponent<AutoHandPlayer>().useMovement = false;
            playerDeathEvent.Raise();
        }
    }

    private void startRegen()
    {
        if (regenCoroutine == null)
        {
            regenCoroutine = StartCoroutine(regenShield());
        }
    }

    private void stopRegen()
    {
        timePassedSinceRecievedDamage = 0;
        if (regenCoroutine != null)
        {
            StopCoroutine(regenCoroutine);
            regenCoroutine = null;
        }
    }

    private IEnumerator regenShield()
    {
        while (timePassedSinceRecievedDamage > 0) 
        {
            if (shield < maxShield)
            {
                float finalHealing = shieldToRegeneratePerLoop + shield;
                changeShield(finalHealing);
            }
            else
            {
                stopRegen();
            }
            yield return new WaitForSeconds(secondsBetweenRegens);
        }
    }

    private void changeHp(float newValue)
    {
        if (newValue <= 0)
        {
            this.Hp = 0;
            die();
        }
        else if(newValue > maxHp)
        {
            this.Hp = maxHp;
        }
        else
        {
            this.Hp = newValue;
        }
        //hacer cosas de GUI
    }

    private void changeShield(float newValue)
    {
        if (newValue <= 0)
        {
            this.shield = 0;
        }
        else if (newValue > maxShield)
        {
            this.shield = maxShield;
        }
        else
        {
            this.shield = newValue;
        }
        //hacer cosas de GUI
    }

    public void revive()
    {
        myState = states.idle;
        this.gameObject.GetComponent<AutoHandPlayer>().useMovement = true;
        changeHp(10);
    }

    public void tryAssignWeapon()
    {
        //this.GetComponentInChildren<Weapon>().assignOwner(this.gameObject);
        StartCoroutine(AssignWeaponWithDelay());
    }

    public IEnumerator AssignWeaponWithDelay()
    {
        yield return new WaitForSeconds(0.1f);
        if (this.transform.parent.GetComponentInChildren<Weapon>() != null)
        {
            this.myWeapon = this.transform.parent.GetComponentInChildren<Weapon>();
            myWeapon.assignOwner(this.gameObject);
        }
    }

    public void unassignWeapon()
    {
        this.myWeapon = null;
    }
}
