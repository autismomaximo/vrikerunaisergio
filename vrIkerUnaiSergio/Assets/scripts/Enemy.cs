using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform target;
    //public Weapon myWeapon;
    public EnemyState myState;
    public GameObject colliderAtaque;

    public float enemyDistanceToAttack;

    public enum EnemyState
    {
        IDLE,
        FOLLOW,
        ATTACK,
        CONFIRMEDATTACK,
        RUN,
        POSITION,
        SUMMON,
        EXPLODE,
        DECIDE,
        DEAD,
    }


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<NavMeshAgent>().enabled = false;
        target = GameObject.Find("AutoHandPlayer").transform;
        //this.GetComponent<NavMeshAgent>().SetDestination(target.position);
        //Debug.Log("Mi vida: " + vida + " STATS: " + stats.vida);
        this.myState = EnemyState.FOLLOW;
        this.GetComponent<NavMeshAgent>().enabled = true;
        StartCoroutine(MaquinamyStates());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator MaquinamyStates()
    {

        while (this.myState != EnemyState.DEAD)
        {
            yield return new WaitForSeconds(0.5f);
            if (target != null)
            {
                if (target.GetComponent<combatCharacter>() != null && target.GetComponent<combatCharacter>().myState == combatCharacter.states.dead)
                {
                    //pos dejo de perseguirle e intento buscar otro player
                    target = null;
                }
                else
                {
                    switch (myState)
                    {
                        case EnemyState.FOLLOW:
                            //Debug.Log("distance: " + Vector3.Distance(this.gameObject.transform.position, target.position));
                            if (Vector3.Distance(this.gameObject.transform.position, target.position) < enemyDistanceToAttack)
                            {
                                myState = EnemyState.ATTACK;
                            }
                            else
                            {
                                //Debug.Log("HUMMMMM");
                                this.GetComponent<NavMeshAgent>().SetDestination(target.position);
                                //this.GetComponent<Animator>().Play("Walk Forward In Place");
                            }

                            break;
                        case EnemyState.ATTACK:
                            //Debug.Log("Ataco");


                            //this.GetComponent<Animator>().Play("Stab Attack");
                            this.GetComponent<NavMeshAgent>().isStopped = true;
                            //this.GetComponent<NavMeshAgent>().enabled = false;
                            this.transform.LookAt(new Vector3(target.position.x, this.transform.position.y, target.transform.position.z));
                            //w1 = Instantiate(warning);
                            //w1.transform.position = this.transform.position + this.transform.forward * 4;
                            //w1.transform.LookAt(this.transform);
                            //w1.transform.parent = this.transform;
                            yield return new WaitForSeconds(1.5f);
                            //Destroy(w1);
                            //this.GetComponent<AudioSource>().Play();
                            myState = EnemyState.CONFIRMEDATTACK;
                            //Debug.Log("BUM!!!");

                            colliderAtaque.SetActive(true);

                            //esto esta algo feo xd
                            //myWeapon.balasActuales = myWeapon.balasMaximas;
                            //myWeapon.shoot();
                            //while(myWeapon.estado != Weapon.state.idle)
                            //{
                            //    yield return new WaitForSeconds(1f);
                            //}

                            //this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                            //this.gameObject.GetComponent<Rigidbody>().velocity = this.transform.forward * 50;
                            yield return new WaitForSeconds(0.2f);
                            colliderAtaque.SetActive(false);
                            //this.gameObject.GetComponent<Rigidbody>().velocity = this.transform.forward * 0;
                            //this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                            this.GetComponent<NavMeshAgent>().enabled = true;
                            this.GetComponent<NavMeshAgent>().isStopped = false;
                            //this.GetComponent<Animator>().Play("Idle");
                            this.myState = EnemyState.FOLLOW;
                            break;
                    }
                }

            }

        }

    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (myState == EnemyState.CONFIRMEDATTACK && collision.gameObject.tag == "Player")
    //    {
    //        //collision.gameObject.GetComponent<Movimientotarget>().getHurt(this.damage, this.force);
    //    }
    //}

    //public override void GetHurt(int dmg, float force)
    //{
        //if (dmg == 0)
        //{
        //    dmg = 1;
        //}

        //this.vida -= dmg;
        //base.ThrowNum(dmg);
        ////Debug.Log("Me han golpeado: " + vida);
        //if (this.vida <= 0 && alive)
        //{
        //    this.alive = false;
        //    StopAllCoroutines();
        //    this.GetComponent<NavMeshAgent>().enabled = false;
        //    if (w1 != null)
        //    {
        //        Destroy(w1);
        //    }
        //    StartCoroutine("Morirme");
        //}
        //else if (alive)
        //{
        //    if (force > forceResist)
        //    {
        //        StartCoroutine(Empuje(force));
        //    }
        //    this.GetComponent<Animator>().Play("Take Damage");
        //}

    //}

    //IEnumerator Empuje(float force)
    //{
    //    this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
    //    this.GetComponent<Rigidbody>().AddForce(target.transform.forward * force, ForceMode.Impulse);
    //    yield return new WaitForSeconds(0.3f);
    //    this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    //}

    //IEnumerator Morirme()
    //{
    //    this.GetComponent<NavMeshAgent>().enabled = true;
    //    //this.GetComponent<CapsuleCollider>().enabled = false;
    //    //this.gameObject.tag = "Untagged";
    //    // Debug.Log("Escarabajo DIES");
    //    //this.GetComponent<Animator>().Play("Die");
    //    //morir.Raise();
    //    //StartCoroutine(base.Coins);
    //    yield return new WaitForSeconds(5f);
    //    Destroy(this.gameObject);
    //}


}
