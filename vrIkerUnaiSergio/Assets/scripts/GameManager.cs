using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //deberia tener una lista con los jugadores que haya, si mueren todos se pone en loss, de momento pongo un solo jugador

    public combatCharacter player;
    public GameObject playerObject;

    public int points;
    public int pointsToWin;

    public GameEvent win;

    public GameStates currentState;
    public enum GameStates 
    {
        playing, win, loss
    }

    // Start is called before the first frame update
    void Start()
    {
        currentState = GameStates.playing;
        playerObject = GameObject.Find("AutoHandPlayer");
        player = playerObject.GetComponent<combatCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addPoints(int newPoints)
    {
        this.points+= newPoints;
        if (points + newPoints >= pointsToWin)
        {
            points = pointsToWin;

            changeGameState(GameStates.win);
            win.Raise();
            //cambiar mas adelante
            //ahora mismo esto cambia el texto de ui de player (en este no el evento, el gamestate), el spawner deja de spawnear bichos
        }
    }

    public void changeGameState(GameStates newState)
    {
        Debug.Log("thing");
        this.currentState = newState;
        switch (currentState) 
        { 
            case GameStates.playing:
                Debug.Log("pos reseteamos");
                break;
            case GameStates.win:
                break;
            case GameStates.loss:
                Debug.Log("thing 2");
                AutoHandPlayer handPlayer = playerObject.GetComponent<AutoHandPlayer>();
                handPlayer.useMovement = false;
                //handPlayer.moveAcceleration = 0;
                //handPlayer.maxMoveSpeed = 0;
                break;
            default:
                break;
        }
    }

    public void playerDiedEventListener()
    {
        Debug.Log("PLAYER DED THING");
        //Debug.Log($"a: {player.myState.ToString()}");
        //aqui miraria si estan todos los jugadores muertos o no
        if(player.myState == combatCharacter.states.dead)
        {
            changeGameState(GameStates.loss);
        }
    }

    public void resetGame()
    {
        changeGameState(GameStates.playing);
    }

}
