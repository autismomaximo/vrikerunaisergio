using TMPro;
using UnityEditor.SearchService;
using UnityEngine;

public class UIHandler : MonoBehaviour
{
    private GameManager gameManager;
    public TextMeshPro hpText;
    public TextMeshPro shieldText;
    public TextMeshPro ammoText;
    public TextMeshPro objectiveText;
    public combatCharacter player;
    public GameObject[] buttons;

    // Start is called before the first frame update
    void Start()
    {
        this.player = this.gameObject.GetComponent<combatCharacter>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.currentState == GameManager.GameStates.loss)
        {
            hpText.text = "aaaaaaaaaaaa";
            objectiveText.text = "you lose";
            foreach (var item in buttons)
            {
                item.SetActive(true);
            }
        }
        else
        {
            hpText.text = $"HP: {player.Hp} / {player.maxHp}";
            shieldText.text = $"SHIELD: {player.shield} / {player.maxShield}";
            if (player.myWeapon != null)
            {
                ammoText.text = $"AMMO: {player.myWeapon.balasActuales} / {player.myWeapon.balasMaximas}";
            }
            else
            {
                ammoText.text = "AMMO: no weapon selected!";
            }

            if (gameManager.currentState == GameManager.GameStates.win)
            {
                objectiveText.text = $"Objective: GET THE FUCK OUT!!!";
            }
            else
            {
                objectiveText.text = $"Objective: {gameManager.points} / {gameManager.pointsToWin}";
            }
        }
        
    }

    public void ResetGame()
    {
        gameManager.resetGame();
    }
}
