using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static Enemy;
using static UnityEngine.GraphicsBuffer;

public class Turret : combatCharacter
{

    int buildCount = 0;
    public int buildCountMax = 5;
    public GameObject[] turretParts;
    public Vector3 builtRotation = new Vector3(0,0,0);
    // Weapon myWeapon;

    public enum turretStates
    {
        broken, searching, shooting
    }
    public turretStates myTurretState = turretStates.broken;
    
    public GameObject searchRadius;

    public Transform targetEnemy;
    public GameObject rotator;
    public float RotationSpeed;

    //values for internal use
    private Quaternion _lookRotation;
    private Vector3 _direction;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(myState);
        switch (myTurretState) 
        { 
            case turretStates.broken:
                
                break; 
            case turretStates.searching:
                
                break; 
            case turretStates.shooting:
                shoot();
                break;   
            default:
                
                break;
        }
    }

    public override void getHurt(combatCharacter chara)
    {
        if (chara.myTeam == team.players)
        {
            cambiarEstado(turretStates.broken);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Hammer>() != null && myTurretState == turretStates.broken && !this.GetComponent<Grabbable>().IsHeld())
        {
            buildTurret();
        }
    }

    public void buildTurret()
    {
        if (buildCount <= buildCountMax)
        {
            this.GetComponent<Grabbable>().enabled = false;
            this.GetComponent<Rigidbody>().isKinematic = true;
            this.transform.rotation = Quaternion.identity;
            /*
            int partsToBuildPerHammerHit = turretParts.Length / buildCountMax;

            for (int i = 0; i < partsToBuildPerHammerHit; i++)
            {
                if(i+buildCount < turretParts.Length)
                    turretParts[i+buildCount].SetActive(true);
            }*/

            if (buildCount < turretParts.Length)
            {
                turretParts[buildCount].SetActive(true);
            }


            buildCount++;


            if(buildCount == buildCountMax)
            {
                turretParts[0].gameObject.transform.eulerAngles = builtRotation;
                this.GetComponent<Grabbable>().enabled = false;
                cambiarEstado(turretStates.searching);
            }


            //Debug.Log("count: "+buildCount+" partsto: "+partsToBuildPerHammerHit);


            /*
            if (this.transform.GetChild(0).gameObject.activeSelf == true)
            {
                if (this.transform.GetChild(0).GetChild(0).gameObject.activeSelf == true)
                {

                }
                else
                {
                    this.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                    this.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
                }
            }
            else
            {
                this.transform.GetChild(0).gameObject.SetActive(true);
            }
            */
        }
    }

    public void cambiarEstado(turretStates newState)
    {
        myTurretState = newState;
        switch (myTurretState)
        {
            case turretStates.broken:
                searchRadius.SetActive(false);
                buildCount = 0;
                foreach (GameObject turretPart in turretParts)
                {
                    turretPart.SetActive(false);
                }
                this.GetComponent<Grabbable>().enabled = true;
                this.GetComponent<Rigidbody>().isKinematic = false;
                break;
            case turretStates.searching:
                searchRadius.SetActive(true);
                search();
                //Debug.Log("aaa");
                break;
            case turretStates.shooting:
                searchRadius.SetActive(false);
                //Debug.Log("bbb");
                break;
            default:
                searchRadius.SetActive(false);
                break;
        }
    }

    public void search()
    {
        //turretParts[0].transform.rotation.rota
        
    }


    public void shoot()
    {
        if (targetEnemy != null)
        {
            //searchRadius.SetActive(false);
            //find the vector pointing from our position to the target
            _direction = (targetEnemy.position - rotator.transform.position).normalized;

            //create the rotation we need to be in to look at the target
            _lookRotation = Quaternion.LookRotation(_direction);

            //rotate us over time according to speed until we are in the required rotation
            rotator.transform.rotation = Quaternion.Slerp(rotator.transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);

            //shoot
            myWeapon.shoot();

            float radius = searchRadius.GetComponent<SphereCollider>().radius * Mathf.Max(searchRadius.transform.lossyScale.x, searchRadius.transform.lossyScale.y, searchRadius.transform.lossyScale.z);

            //check if enemy wanders off
            if (targetEnemy == null || Vector3.Distance(this.gameObject.transform.position, targetEnemy.position) > radius)
            {
                cambiarEstado(turretStates.searching);
            }
        }
        else
        {
            cambiarEstado(turretStates.searching);
        }
    }

}
